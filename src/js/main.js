$('[data-fancybox="gallery"]').fancybox({
});

$(function() {
    $("#changethewords").changeWords({
        time: 1500,
        animate: "zoomIn",
        selector: "span",
        repeat:true
    });
});

(function (w, d) {
    w.onload = function () {
        const wp = d.getElementById('animated-background');
        const image = ['/img/first-slide/2.jpg', '/img/first-slide/3.jpg', '/img/first-slide/1.jpg'];
        const ln = image.length;

        const changeImage = function (i) {
            if (i < ln) {
                setTimeout(function () {
                    wp.style.backgroundImage = `url(${image[i]})`;
                    i++;
                    changeImage(i);
                }, 5000);
            } else {
                changeImage(0);
            }
        };
        changeImage(0);
    };
}(window, document));